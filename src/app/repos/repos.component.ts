import { Component, OnInit } from '@angular/core';
import { RepoService } from './repo.service';

@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: ['./repos.component.css']
})
export class ReposComponent implements OnInit {

	repositories : any = [];

  constructor(private repoService: RepoService) { }

  ngOnInit() {
  	this.repoService.getRepos().subscribe((d)=> this.repositories = d.json() );
  }

}
